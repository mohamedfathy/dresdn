<?php 

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Complaint;

class FrontController extends Controller 
{
    public function home () 
    {
        return view('Front.home');
    }   

    public function complaints (Request $request) 
    {
        $validatedData = $request->validate([
            'name' => 'required|min:3|max:100',
            'phone' => 'required|min:3|max:45|regex:/^\+?\d[0-9-]{6,15}$/',
            'message' => 'required|min:3'
        ]);

        $Complaint = new Complaint();
        $Complaint->name = $request->name;
        $Complaint->phone = $request->phone;
        $Complaint->message = $request->message;
        $Complaint->save();

        return redirect('/');
    }

    public function portfolio ()
    {
        return view('Front.portfolio');
    }

    public function services ()
    {
        return view('Front.services');
    }

    public function about ()
    {
        return view('Front.about');
    }

    public function team ()
    {
        return view('Front.team');
    }

    public function contact ()
    {
        return view('Front.contact');
    }
}