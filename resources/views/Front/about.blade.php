@extends('Front.layouts.master')
@section('title', 'About Us')
@section('content')
<!-- main section -->
<section class="main-section bg-cover d-flex align-items-center" style="background-image: url('assets/images/cover2.png')">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 mb-4 order-2 order-md-1">
                <div class="intro">
                    <h1 class="heading">ABOUT US</h1>
                    <p>Our start is always our motivation</p>
                </div>
            </div>
            <div class="col-md-6 mb-4 order-1 order-md-2">
                <div class="home-image">
                    <img src="assets/images/about-us/1.png" alt="about-us-image">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end main section -->

<!-- our story section -->
<section class="our-story">
    <div class="container">
        <div class="heading text-center">
            <h2 class="heading-text">OUR STORY</h2>
        </div>

        <div class="row align-items-center justify-content-center">
            <div class="col-md-10 col-lg-6 m-b-30">
                <div class="logo-image">
                    <img src="assets/images/about-us/logo.png" alt="logo-image">
                </div>
            </div>

            <div class="col-md-10 col-lg-6 m-b-30">
                <div class="intro">
                    <p class="intro-text"> Lorem Ipsum is simply dummy text of the printing and typesetting
                        industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when
                        an unknown printer took a galley of type and scrambled it to make a type specimen book. It
                        has survived not only five centuries, but also the leap into electronic typesetting,
                        remaining essentially unchanged. It was popularised in the 1960s with the release of
                        Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing
                        software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy
                        text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard
                        dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
                        it to make a type specimen book. It has survived not only five centuries, but also the leap
                        into electronic typesetting</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end our story section -->

<!-- banner section -->
<section class="banner bg-cover" style="background-image: url('assets/images/about-us/2.png')"></section>
<!-- end banner section -->

<!-- testimonials section -->
<section class="testimonials text-center">
<div class="container">
    @include('Front.partials.testimonials')
</div>
</section>
<!-- end testimonials section -->

<!-- contact-us section -->
<section class="contact-us bg-cover" style="background-image: url('assets/images/contact-us-bg.png')">
<div class="container">
    @include('Front.partials.complaints')
</div>
</section>
<!-- end contact-us section -->

<!-- map section -->
<section class="map">
    @include('Front.partials.map')
</section>
<!--end map section -->


<script src="assets/js/vendors/jquery-3.1.1.min.js"></script>
<script src="assets/js/vendors/slick.min.js"></script>
<script src="assets/js/vendors/bootstrap.min.js"></script>

<script src="assets/js/main.js"></script>
@endsection