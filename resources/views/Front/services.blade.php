@extends('Front.layouts.master')
@section('title', 'Services')
@section('content')
<!-- main section -->
<section class="main-section bg-cover d-flex align-items-center" style="background-image: url('assets/images/cover2.png')">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 mb-4 order-2 order-md-1">
                <div class="intro">
                    <h1 class="heading">OUR SERVICES</h1>
                    <p>Our works are designed by a team of experts whose goal is always to achieve the best results
                        ever.</p>
                </div>
            </div>
            <div class="col-md-6 mb-4 order-1 order-md-2">
                <div class="home-image">
                    <img src="assets/images/services.png" alt="services-image">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end main section -->

<!-- services section -->
<section class="our-services text-center">
    <div class="container">
        <div class="row">
            <!-- service -->
            <div class="col-sm-6 col-md-4 m-b-30">
                <div class="service">
                    <img src="assets/images/our-services/mobile.png" alt="service-image">
                    <h3 class="service-title">Mobile Apps</h3>
                </div>
            </div>
            <!-- end service -->

            <!-- service -->
            <div class="col-sm-6 col-md-4 m-b-30">
                <div class="service">
                    <img src="assets/images/our-services/web.png" alt="service-image">
                    <h3 class="service-title">Websites</h3>
                </div>
            </div>
            <!-- end service -->

            <!-- service -->
            <div class="col-sm-6 col-md-4 m-b-30">
                <div class="service">
                    <img src="assets/images/our-services/graphic.png" alt="service-image">
                    <h3 class="service-title">Graphic Design</h3>
                </div>
            </div>
            <!-- end service -->

        </div>
    </div>
</section>
<!-- end services section -->

<!-- article section -->
<section class="article">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 m-b-30 article-cover">
                <div class="article-image">
                    <img src="assets/images/our-services/websites.png" alt="article-image">
                </div>
            </div>

            <div class="col-md-6 m-b-30 article-content">
                <div>
                    <h2 class="article-title">Mobile Apps</h2>
                    <p class="article-text">Lorem Ipsum is simply dummy text of the printing and typesetting
                        industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when
                        an unknown printer took a galley of type and scrambled it to make a type specimen book. It
                        has survived not only five centuries, but also the leap into electronic typesetting,
                        remaining essentially unchanged. It was popularised in the 1960s with the release of
                        Letraset sheets containing Lorem</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end article section -->

<!-- article section -->
<section class="article">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 m-b-30 article-cover">
                <div class="article-image">
                    <img src="assets/images/our-services/websites.png" alt="article-image">
                </div>
            </div>

            <div class="col-md-6 m-b-30 article-content">
                <div>
                    <h2 class="article-title">Mobile Apps</h2>
                    <p class="article-text">Lorem Ipsum is simply dummy text of the printing and typesetting
                        industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when
                        an unknown printer took a galley of type and scrambled it to make a type specimen book. It
                        has survived not only five centuries, but also the leap into electronic typesetting,
                        remaining essentially unchanged. It was popularised in the 1960s with the release of
                        Letraset sheets containing Lorem</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end article section -->

<!-- article section -->
<section class="article">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 m-b-30 article-cover">
                <div class="article-image">
                    <img src="assets/images/our-services/websites.png" alt="article-image">
                </div>
            </div>

            <div class="col-md-6 m-b-30 article-content">
                <div>
                    <h2 class="article-title">Mobile Apps</h2>
                    <p class="article-text">Lorem Ipsum is simply dummy text of the printing and typesetting
                        industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when
                        an unknown printer took a galley of type and scrambled it to make a type specimen book. It
                        has survived not only five centuries, but also the leap into electronic typesetting,
                        remaining essentially unchanged. It was popularised in the 1960s with the release of
                        Letraset sheets containing Lorem</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end article section -->

<!-- contact-us section -->
<section class="contact-us bg-cover" style="background-image: url('assets/images/contact-us-bg.png')">
<div class="container">
    @include('Front.partials.complaints')
</div>
</section>
<!-- end contact-us section -->
@endsection