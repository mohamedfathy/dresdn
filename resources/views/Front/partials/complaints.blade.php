<div class="heading text-center">
<h2 class="heading-text">LET'S TALK</h2>
</div>
@if ($errors->any())
    <div class="alert alert-primary">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form class="col-lg-8 m-auto" method="post" action="/complaints">
    @csrf
    <div class="form-group mb-4">
        <input type="text" class="form-control input" placeholder="Name" name="name" value="{{ old('name') }}">
    </div>
    <div class="form-group mb-4">
        <input type="tel" class="form-control input" placeholder="Mobile Number" name="phone" value="{{ old('phone') }}">
    </div>
    <div class="form-group mb-4">
        <textarea class="form-control textarea" placeholder="Your message" name="message" >{{ old('message') }}</textarea>
    </div>
    <div class="form-group text-center">
        <button type="submit" class="btn main-btn white-btn px-5">Submit</button>
    </div>
</form>