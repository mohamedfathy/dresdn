<div class="heading text-center">
    <h2 class="heading-text">TESTIMONIALS</h2>
    </div>
    <div class="testimonials-slider">
    <!-- slider item -->
    <div class="slider-item col-md-10 col-lg-7 m-auto bg-cover"
        style="background-image: url('assets/images/testimonials-cover.png')">
        <img
        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSFU0Brl89fkOR81nL0jXAlu21JZrePsLp9hqUPG476SFMN8lsj6A"
        alt="person-image" class="person-image">
        <h2 class="person-name">Ahmed Ali</h2>
        <p class="text"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
        been the
        industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
        scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
        into electronic typesetting, remaining essentially</p>
    </div>
    <!-- end slider item -->

    <!-- slider item -->
    <div class="slider-item col-md-10 col-lg-7 m-auto bg-cover"
        style="background-image: url('assets/images/testimonials-cover.png')">

        <img
        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSFU0Brl89fkOR81nL0jXAlu21JZrePsLp9hqUPG476SFMN8lsj6A"
        alt="person-image" class="person-image">
        <h2 class="person-name">Ahmed Ali</h2>
        <p class="text"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
        been the
        industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
        scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
        into electronic typesetting, remaining essentially</p>
    </div>
    <!-- end slider item -->

    <!-- slider item -->
    <div class="slider-item col-md-10 col-lg-7 m-auto bg-cover"
        style="background-image: url('assets/images/testimonials-cover.png')">

        <img
        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSFU0Brl89fkOR81nL0jXAlu21JZrePsLp9hqUPG476SFMN8lsj6A"
        alt="person-image" class="person-image">
        <h2 class="person-name">Ahmed Ali</h2>
        <p class="text"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
        been the
        industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
        scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
        into electronic typesetting, remaining essentially</p>
    </div>
    <!-- end slider item -->
    </div>