<div id="mapCanvas"></div>
    <!-- Code For Google Maps -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC1kz6gxC9-ZrMKUATK4C4YdtmqqxBEG4o&callback=initialize"></script>
    <script type="text/javascript">
    //Custom marker
    var image = 'place-marker.png';
    // Initialize the Class
    var geocoder = new google.maps.Geocoder();

    //Set the Default Latitute And Longtiude for the map
    var Lat = 30.095231; 
    var lang = 31.333115; 
    // Get the long and latitude Position
    function geocodePosition(pos) {
    geocoder.geocode({
        latLng: pos
    }, function(responses) {
        if (responses && responses.length > 0) {
        updateMarkerAddress(responses[0].formatted_address);
        } else {
        updateMarkerAddress('Cannot determine address at this location.');
        }
    });
    }

    // Update the long and latitude Position
    function updateMarkerPosition(latLng) {
        lat = latLng.lat();
        lang = latLng.lng();
        
        document.getElementById('latitude').value = lat;
        document.getElementById('logitude').value = lang; 
    }

    // Update the Marker Address
    function updateMarkerAddress(str) {
    document.getElementById('address').innerHTML = str;
    document.getElementById('address1').value = str;
    }

    // Initializition function
    function initialize() {
    var mapOptions = {
        // How zoomed in you want the map to start at (always required)
        zoom: 14,
    
        // The latitude and longitude to center the map (always required)
        center: new google.maps.LatLng( 30.095231, 31.333115), // New York
    // 24.666515, 46.729875
    // 40.6700, -73.9400
        // How you would like to style the map. 
        // This is where you would paste any style found on Snazzy Maps.
        styles: [
    {
        "featureType": "landscape.natural",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#e0efef"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "hue": "#1900ff"
            },
            {
                "color": "#c0e8e8"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "lightness": 100
            },
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "lightness": 700
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#7dcdcd"
            }
        ]
    }
]
    };
    var latLng = new google.maps.LatLng(30.095231, 31.333115);
    var map = new google.maps.Map(document.getElementById('mapCanvas'), mapOptions);
    var marker = new google.maps.Marker({
        position: latLng,
        title: 'Dresdn Company - Software House',
        map: map,
        //icon: image,
        draggable: false
    });

    // Update current position info.
    updateMarkerPosition(latLng);
    geocodePosition(latLng);

    // Add dragging event listeners.
    google.maps.event.addListener(marker, 'dragstart', function() {
        updateMarkerAddress('Dragging...');
    });

    google.maps.event.addListener(marker, 'drag', function() {
        //updateMarkerStatus('Dragging...');
        updateMarkerPosition(marker.getPosition());
    });

    google.maps.event.addListener(marker, 'dragend', function() {
        //updateMarkerStatus('Drag ended');
        geocodePosition(marker.getPosition());
    });
    }

    // Onload handler to fire off the app.
    google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <style>
    #mapCanvas {
        position: relative;
        height: 400px;
        width: 100%;
        overflow: hidden;
    }
    </style>