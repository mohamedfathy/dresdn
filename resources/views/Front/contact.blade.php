@extends('Front.layouts.master')
@section('title', 'Contact')
@section('content')
<!-- main section -->
<section class="main-section contact bg-cover d-flex align-items-center" style="background-image: url('assets/images/cover2.png')">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 mb-4 order-2 order-md-1">
                <div class="intro">
                    <h1 class="heading">CONTACT US</h1>
                    <p>Our works are designed by a team of experts whose goal is always to achieve the best results
                        ever.</p>
                </div>
            </div>
            <div class="col-md-6 mb-4 order-1 order-md-2">
                <div class="home-image">
                    <img src="assets/images/contact-us.png" alt="about-us-image">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end main section -->

<!-- contact info section -->
<section class="contact-info text-center">
    <div class="container">
        <div class="heading text-center">
            <h2 class="heading-text">we will be in touch</h2>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-5 m-b-30">
                <div class="phones box">
                    <div class="icon d-flex"><i class="fas fa-phone"></i></div>
                    <ul class="list-unstyled phones-links">
                        <li><a href="tel:+20 155 233 223">+20 155 007 2987 </a></li>
                        <li><a href="tel:+20 155 233 223">+20 106 667 4627</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-6 col-lg-5 m-b-30">
                <div class="emails box">
                    <div class="icon d-flex"><i class="fas fa-envelope"></i></div>
                    <ul class="list-unstyled email-links">
                        <li><a href="mailto:sales@dresdn.com">sales@dresdn.com</a></li>
                        <li><a href="mailto:career@dresdn.com">career@dresdn.com</a></li>
                        <li><a href="mailto:support@dresdn.com">support@dresdn.com</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end contact info section -->
<!-- contact-us section -->
<section class="contact-us bg-cover" style="background-image: url('assets/images/contact-us-bg.png')">
<div class="container">
    @include('Front.partials.complaints')
</div>
</section>
<!-- end contact-us section -->

<!-- map section -->
<section class="map">
    @include('Front.partials.map')
</section>
<!--end map section -->


<script src="assets/js/vendors/jquery-3.1.1.min.js"></script>
<script src="assets/js/vendors/slick.min.js"></script>
<script src="assets/js/vendors/bootstrap.min.js"></script>

<script src="assets/js/main.js"></script>
@endsection