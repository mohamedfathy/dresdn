  <!-- footer -->
  <footer class="footer bg-cver" style="background-image: url('assets/images/footer.png')">
    <div class="container">
      <div class="row justify-content-between">
        <div class="col-lg-9 order-lg-1 order-2">
          <div class="content">
            <div class="row justify-content-between">
              <div class="col-md-6">
                <div class="links">
                  <h5>Contact us</h5>
                  <ul class="list-unstyled">
                    <li><a href="#"><i class="fas fa-phone mx-1"></i> +20 155 007 2987 - +20 106 667 4627</a></li>
                    <li><a href="#"><i class="fab fa-whatsapp mx-1"></i> +20 155 007 2987 - +20 106 667 4627</a></li>
                    <li><a href="#"><i class="fas fa-envelope mx-1"></i> sales@dresdn.com - career@dresdn.com</a></li>
                    <li><a href="#"><i class="fas fa-map-marker-alt mx-1"></i> 12 Mourad Bek, Almazah, Helioplis, Cairo</a></li>
                  </ul>
                </div>
              </div>

              

              <div class="col-md-5">
                <div class="links">
                  <h5>Brows</h5>
                  <ul class="list-unstyled">
                    <li><a href="/portfolio">Our works</a></li>
                    <li><a href="/services">Our services</a></li>
                    <li><a href="/about">About us</a></li>
                    <li><a href="/team">Our Team</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-3 order-lg-2 order-1">
          <div class="intro ">
            <img src="assets/images/footer-logo.png" alt="logo-image" class="logo">
            <ul class="list-unstyled d-flex align-items-center  social-icons">
              <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
              <li><a href="#"><i class="fab fa-twitter"></i></a></li>
              <li><a href="#"><i class="fab fa-instagram"></i></a></li>
              <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
              <li><a href="#"><i class="fab fa-behance"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="copyright text-center">
      <p>Copyright © 2019- <?php echo date('Y'); ?> Dresdn Company, All rights reserved.</p>
    </div>
  </footer>
  <!-- end footer -->