<body>
  <!-- header -->
  <header class="header">
    <!-- main-navbar -->
    <nav class="navbar navbar-expand-lg navbar-light main-navbar">
      <div class="container">
        <a class="navbar-brand" href="/" style="background-image: url('assets/images/logo.png')"></a>
        <div class="menu-icon navbar-toggler" data-toggle="collapse" data-target="#navbarSupportedContent">
          <span></span>
          <span></span>
          <span></span>
        </div>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- nav links -->
          <ul class="navbar-nav ml-auto">
            <li class="nav-item {{ Request::is('/') ? 'active ' : '' }}">
                <a class="nav-link" href="/">Home</a>
            </li>
            <li class="nav-item {{ Request::is('portfolio') ? 'active ' : '' }}">
                <a class="nav-link" href="/portfolio">Portfolio</a>
            </li>
            <li class="nav-item {{ Request::is('services') ? 'active ' : '' }}">
                <a class="nav-link" href="/services">Our Services</a>
            </li>
            <li class="nav-item {{ Request::is('about') ? 'active ' : '' }}">
                <a class="nav-link" href="/about">About Us</a>
            </li>
            <li class="nav-item {{ Request::is('team') ? 'active ' : '' }}">
                <a class="nav-link" href="/team">Our Team</a>
            </li>
            <li class="nav-item {{ Request::is('contact') ? 'active ' : '' }}">
                <a class="nav-link" href="/contact">Contact Us</a>
            </li>
        </ul>
        <!-- end nav links -->

        </div>
      </div>
    </nav>
    <!-- end main-navbar -->
  </header>
  <!-- end header -->