@extends('Front.layouts.master')
@section('title', 'Home')
@section('content')
<!-- main section -->
<section class="main-section bg-cover d-flex align-items-center"
style="background-image: url('assets/images/home-cover.png')">
<div class="container">
    <div class="row align-items-center">
    <div class="col-md-6 mb-4 order-2 order-md-1">
        <div class="intro">
        <h1 class="heading">Perfect Is Possible</h1>
        <p>We always challenge ourselves to provide a top quality in our applications.</p>
        <a href="/portfolio" class="btn main-btn white-btn">Explore our experience <i
            class="fas fa-chevron-right mx-1"></i></a>
        </div>
    </div>
    <div class="col-md-6 mb-4 order-1 order-md-2">
        <div class="home-image">
        <img src="assets/images/home-image.png" alt="home-image">
        </div>
    </div>
    </div>
    <div class="text-center">
    <button class="scroll-btn"><i class="fas fa-chevron-down"></i></button>
    </div>
</div>
</section>
<!-- end main section -->

<!-- services section -->
<section class="services">
<div class="container">
    <div class="heading text-center">
    <h2 class="heading-text">SERVICES</h2>
    </div>
    <div class="col-xl-10 p-0 m-auto">
    <div class="row justify-content-center">
        <!-- start card -->
            <div class="col-sm-8 col-md-6 col-lg-4 m-b-30">
        <div class="card service">
            <div class="bg-cover card-header" style="background-image: url('assets/images/service-bg.png')"></div>
            <div class="card-body">
            <img src="assets/images/services/mobile.png" alt="service-image" class="card-image">
            <h5 class="card-title">Mobile Apps</h5>
            <p class="card-text">Bring your application to reality. Our company offers creative and innovative expertise on Mobile Apps across all mobile platforms. </p>
            <a href="/services" class="btn main-btn">Details</a>
            </div>
        </div>
        </div>
        <!-- end start card -->

        <!-- start card -->
        <div class="col-sm-8 col-md-6 col-lg-4 m-b-30">
        <div class="card service">
            <div class="bg-cover card-header" style="background-image: url('assets/images/service-bg.png')"></div>
            <div class="card-body">
            <img src="assets/images/services/websites.png" alt="service-image" class="card-image">
            <h5 class="card-title">Websites</h5>
            <p class="card-text">Build your own customized website. Our company offers simple user experience and rubust websites that users will love.</p>
            <a href="/services" class="btn main-btn">Details</a>
            </div>
        </div>
        </div>
        <!-- end start card -->

        <!-- start card -->
        <div class="col-sm-8 col-md-6 col-lg-4 m-b-30">
        <div class="card service">
            <div class="bg-cover card-header" style="background-image: url('assets/images/service-bg.png')"></div>
            <div class="card-body">
            <img src="assets/images/services/graphics.png" alt="service-image" class="card-image">
            <h5 class="card-title">Graphic Design</h5>
            <p class="card-text">Show your character to the World. Our company offers Micro services Logo, Motion Graphic and etc...</p>
            <a href="/services" class="btn main-btn">Details</a>
            </div>
        </div>
        </div>
        <!-- end start card -->
    </div>
    </div>
</div>
</section>
<!-- end services section -->

<!-- portfolio section -->
<section class="portfolio">
<div class="row no-gutters">
    <div class="col-sm-6 col-lg-4">
    <div class="portfolio-item">
        <div class="image-container">
        <img src="assets/images/portfolio/1.png" alt="portfolio-image">
        </div>
        <div class="intro overlay d-flex">
        <div class="content m-auto">
            <h2 class="title">PORTFOLIO</h2>
            <a href="/portfolio" class="btn main-btn white-btn">ٍSee Works <i class="fas fa-chevron-right ml-3"></i></a>
        </div>
        </div>
    </div>
    </div>

    <!-- portfolio item -->
    <div class="col-sm-6 col-lg-4">
    <div class="portfolio-item">
        <div class="image-container">
        <img src="assets/images/portfolio/dress.png" alt="portfolio-image">
        <div class="overlay work-info">
            <div class="content d-flex flex-column justify-content-around">
            <div class="info">
                <h2 class="title">WeChat App</h2>
                <span class="tag">Mobile app</span>
            </div>
            <div class="d-flex links justify-content-end">
                <a href="#"><i class="fas fa-eye"></i></a>
                <a href="#"><i class="fab fa-apple"></i></a>
                <a href="#"><i class="fab fa-google-play"></i></a>
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>
    <!-- end portfolio item -->

    <!-- portfolio item -->
    <div class="col-sm-6 col-lg-4">
    <div class="portfolio-item">
        <div class="image-container">
        <img src="assets/images/portfolio/el-foad_logo.png" alt="portfolio-image">
        <div class="overlay work-info">
            <div class="content d-flex flex-column justify-content-around">
            <div class="info">
                <h2 class="title">WeChat App</h2>
                <span class="tag">Mobile app</span>
            </div>
            <div class="d-flex links justify-content-end">
                <a href="#"><i class="fas fa-eye"></i></a>
                <a href="#"><i class="fab fa-apple"></i></a>
                <a href="#"><i class="fab fa-google-play"></i></a>
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>
    <!-- end portfolio item -->

    <!-- portfolio item -->
    <div class="col-sm-6 col-lg-4">
    <div class="portfolio-item">
        <div class="image-container">
        <img src="assets/images/portfolio/socia.png" alt="portfolio-image">
        <div class="overlay work-info">
            <div class="content d-flex flex-column justify-content-around">
            <div class="info">
                <h2 class="title">WeChat App</h2>
                <span class="tag">Mobile app</span>
            </div>
            <div class="d-flex links justify-content-end">
                <a href="#"><i class="fas fa-eye"></i></a>
                <a href="#"><i class="fab fa-apple"></i></a>
                <a href="#"><i class="fab fa-google-play"></i></a>
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>
    <!-- end portfolio item -->

    <!-- portfolio item -->
    <div class="col-sm-6 col-lg-4">
    <div class="portfolio-item">
        <div class="image-container">
        <img src="assets/images/portfolio/prev_1_2x.png" alt="portfolio-image">
        <div class="overlay work-info">
            <div class="content d-flex flex-column justify-content-around">
            <div class="info">
                <h2 class="title">WeChat App</h2>
                <span class="tag">Mobile app</span>
            </div>
            <div class="d-flex links justify-content-end">
                <a href="#"><i class="fas fa-eye"></i></a>
                <a href="#"><i class="fab fa-apple"></i></a>
                <a href="#"><i class="fab fa-google-play"></i></a>
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>
    <!-- end portfolio item -->

    <!-- portfolio item -->
    <div class="col-sm-6 col-lg-4">
    <div class="portfolio-item">
        <div class="image-container">
        <img src="assets/images/portfolio/ship_it_app_2x.png" alt="portfolio-image">
        <div class="overlay work-info">
            <div class="content d-flex flex-column justify-content-around">
            <div class="info">
                <h2 class="title">WeChat App</h2>
                <span class="tag">Mobile app</span>
            </div>
            <div class="d-flex links justify-content-end">
                <a href="#"><i class="fas fa-eye"></i></a>
                <a href="#"><i class="fab fa-apple"></i></a>
                <a href="#"><i class="fab fa-google-play"></i></a>
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>
    <!-- en dportfolio item -->
</div>
</section>
<!-- end portfolio section -->

<!-- testimonials section -->
<section class="testimonials text-center">
<div class="container">
    @include('Front.partials.testimonials')
</div>
</section>
<!-- end testimonials section -->

<!-- contact-us section -->
<section class="contact-us bg-cover" style="background-image: url('assets/images/contact-us-bg.png')">
<div class="container">
    @include('Front.partials.complaints')
</div>
</section>
<!-- end contact-us section -->

<!-- map section -->
<section class="map">
    @include('Front.partials.map')
</section>
<!--end map section -->
@endsection