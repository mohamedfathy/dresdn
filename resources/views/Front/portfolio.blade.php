@extends('Front.layouts.master')
@section('title', 'Portfolio')
@section('content')
<!-- main section -->
<section class="main-section bg-cover d-flex align-items-center" style="background-image: url('assets/images/cover2.png')">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 mb-4 order-2 order-md-1">
                <div class="intro">
                    <h1 class="heading">PORTFOLIO</h1>
                    <p>Our works are designed by a team of experts whose goal is always to achieve the best results
                        ever. </p>
                </div>
            </div>
            <div class="col-md-6 mb-4 order-1 order-md-2">
                <div class="home-image">
                    <img src="assets/images/portfolio.png" alt="portfolio-image">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end main section -->

<!-- portfolio section -->
<section class="portfolio">
    <div class="container">
        <!-- filter buttons -->
        <div class="filter-buttons d-flex justify-content-center flex-wrap m-b-30">
            <button data-filter="*" class="btn main-btn m-1 active">ALL</button>
            <button data-filter=".mobile-apps" class="btn main-btn m-1">Mobile apps</button>
            <button data-filter=".web-apps" class="btn main-btn m-1">Web Apps</button>
            <button data-filter=".graphic-design" class="btn main-btn m-1">Graphic Design</button>
        </div>
        <!-- end filter buttons -->

        <div class="portfolio-items">
            <!-- portfolio item -->
            <!-- col-lg-8 for large image -->
            <div class="col-md-6 col-lg-8 float-left item mobile-apps">
                <div class="portfolio-item">
                    <div class="image-container">
                        <img src="assets/images/portfolio/ship_it_app_2x.png" alt="portfolio-image">
                        <div class="overlay work-info">
                            <div class="content d-flex flex-column justify-content-around">
                                <div class="info">
                                    <h2 class="title">WeChat App</h2>
                                    <span class="tag">Mobile app</span>
                                </div>
                                <div class="d-flex links justify-content-end">
                                    <a href="#"><i class="fas fa-eye"></i></a>
                                    <a href="#"><i class="fab fa-apple"></i></a>
                                    <a href="#"><i class="fab fa-google-play"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end portfolio item -->

            <!-- portfolio item -->
            <div class="col-md-6 col-lg-4 float-left item mobile-apps">
                <div class="portfolio-item">
                    <div class="image-container">
                        <img src="assets/images/portfolio/2.png" alt="portfolio-image">
                        <div class="overlay work-info">
                            <div class="content d-flex flex-column justify-content-around">
                                <div class="info">
                                    <h2 class="title">WeChat App</h2>
                                    <span class="tag">Mobile app</span>
                                </div>
                                <div class="d-flex links justify-content-end">
                                    <a href="#"><i class="fas fa-eye"></i></a>
                                    <a href="#"><i class="fab fa-apple"></i></a>
                                    <a href="#"><i class="fab fa-google-play"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end portfolio item -->

            <!-- portfolio item -->
            <div class="col-md-6 col-lg-4 float-left item graphic-design">
                <div class="portfolio-item">
                    <div class="image-container">
                        <img src="assets/images/portfolio/el-foad_logo.png" alt="portfolio-image">
                        <div class="overlay work-info">
                            <div class="content d-flex flex-column justify-content-around">
                                <div class="info">
                                    <h2 class="title">WeChat App</h2>
                                    <span class="tag">Mobile app</span>
                                </div>
                                <div class="d-flex links justify-content-end">
                                    <a href="#"><i class="fas fa-eye"></i></a>
                                    <a href="#"><i class="fab fa-apple"></i></a>
                                    <a href="#"><i class="fab fa-google-play"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end portfolio item -->

            <!-- portfolio item -->
            <div class="col-md-6 col-lg-4 float-left item graphic-design">
                <div class="portfolio-item">
                    <div class="image-container">
                        <img src="assets/images/portfolio/socia.png" alt="portfolio-image">
                        <div class="overlay work-info">
                            <div class="content d-flex flex-column justify-content-around">
                                <div class="info">
                                    <h2 class="title">WeChat App</h2>
                                    <span class="tag">Mobile app</span>
                                </div>
                                <div class="d-flex links justify-content-end">
                                    <a href="#"><i class="fas fa-eye"></i></a>
                                    <a href="#"><i class="fab fa-apple"></i></a>
                                    <a href="#"><i class="fab fa-google-play"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end portfolio item -->

            <!-- portfolio item -->
            <div class="col-md-6 col-lg-4 float-left item web-apps">
                <div class="portfolio-item">
                    <div class="image-container">
                        <img src="assets/images/portfolio/3.png" alt="portfolio-image">
                        <div class="overlay work-info">
                            <div class="content d-flex flex-column justify-content-around">
                                <div class="info">
                                    <h2 class="title">WeChat App</h2>
                                    <span class="tag">Mobile app</span>
                                </div>
                                <div class="d-flex links justify-content-end">
                                    <a href="#"><i class="fas fa-eye"></i></a>
                                    <a href="#"><i class="fab fa-apple"></i></a>
                                    <a href="#"><i class="fab fa-google-play"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end portfolio item -->

            <!-- portfolio item -->
            <div class="col-md-6 col-lg-4 float-left item mobile-apps">
                <div class="portfolio-item">
                    <div class="image-container">
                        <img src="assets/images/portfolio/dress.png" alt="portfolio-image">
                        <div class="overlay work-info">
                            <div class="content d-flex flex-column justify-content-around">
                                <div class="info">
                                    <h2 class="title">WeChat App</h2>
                                    <span class="tag">Mobile app</span>
                                </div>
                                <div class="d-flex links justify-content-end">
                                    <a href="#"><i class="fas fa-eye"></i></a>
                                    <a href="#"><i class="fab fa-apple"></i></a>
                                    <a href="#"><i class="fab fa-google-play"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end portfolio item -->

            <!-- portfolio item -->
            <div class="col-md-6 col-lg-4 float-left item mobile-apps">
                <div class="portfolio-item">
                    <div class="image-container">
                        <img src="assets/images/portfolio/5.png" alt="portfolio-image">
                        <div class="overlay work-info">
                            <div class="content d-flex flex-column justify-content-around">
                                <div class="info">
                                    <h2 class="title">WeChat App</h2>
                                    <span class="tag">Mobile app</span>
                                </div>
                                <div class="d-flex links justify-content-end">
                                    <a href="#"><i class="fas fa-eye"></i></a>
                                    <a href="#"><i class="fab fa-apple"></i></a>
                                    <a href="#"><i class="fab fa-google-play"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end portfolio item -->

            <!-- portfolio item -->
            <div class="col-md-6 col-lg-4 float-left item web-apps">
                <div class="portfolio-item">
                    <div class="image-container">
                        <img src="assets/images/portfolio/prev_1_2x.png" alt="portfolio-image">
                        <div class="overlay work-info">
                            <div class="content d-flex flex-column justify-content-around">
                                <div class="info">
                                    <h2 class="title">WeChat App</h2>
                                    <span class="tag">Mobile app</span>
                                </div>
                                <div class="d-flex links justify-content-end">
                                    <a href="#"><i class="fas fa-eye"></i></a>
                                    <a href="#"><i class="fab fa-apple"></i></a>
                                    <a href="#"><i class="fab fa-google-play"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end portfolio item -->

            <!-- portfolio item -->
            <div class="col-md-6 col-lg-4 float-left item web-apps">
                <div class="portfolio-item">
                    <div class="image-container">
                        <img src="assets/images/portfolio/4.png" alt="portfolio-image">
                        <div class="overlay work-info">
                            <div class="content d-flex flex-column justify-content-around">
                                <div class="info">
                                    <h2 class="title">WeChat App</h2>
                                    <span class="tag">Mobile app</span>
                                </div>
                                <div class="d-flex links justify-content-end">
                                    <a href="#"><i class="fas fa-eye"></i></a>
                                    <a href="#"><i class="fab fa-apple"></i></a>
                                    <a href="#"><i class="fab fa-google-play"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end portfolio item -->

            <!-- portfolio item -->
            <div class="col-md-6 col-lg-4 float-left item mobile-apps">
                <div class="portfolio-item">
                    <div class="image-container">
                        <img src="assets/images/portfolio/8.png" alt="portfolio-image">
                        <div class="overlay work-info">
                            <div class="content d-flex flex-column justify-content-around">
                                <div class="info">
                                    <h2 class="title">WeChat App</h2>
                                    <span class="tag">Mobile app</span>
                                </div>
                                <div class="d-flex links justify-content-end">
                                    <a href="#"><i class="fas fa-eye"></i></a>
                                    <a href="#"><i class="fab fa-apple"></i></a>
                                    <a href="#"><i class="fab fa-google-play"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end portfolio item -->

            <!-- portfolio item -->
            <div class="col-md-6 col-lg-4 float-left item web-apps">
                <div class="portfolio-item">
                    <div class="image-container">
                        <img src="assets/images/portfolio/7.png" alt="portfolio-image">
                        <div class="overlay work-info">
                            <div class="content d-flex flex-column justify-content-around">
                                <div class="info">
                                    <h2 class="title">WeChat App</h2>
                                    <span class="tag">Mobile app</span>
                                </div>
                                <div class="d-flex links justify-content-end">
                                    <a href="#"><i class="fas fa-eye"></i></a>
                                    <a href="#"><i class="fab fa-apple"></i></a>
                                    <a href="#"><i class="fab fa-google-play"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end portfolio item -->

            <!-- portfolio item -->
            <div class="col-md-6 col-lg-4 float-left item mobile-apps">
                <div class="portfolio-item">
                    <div class="image-container">
                        <img src="assets/images/portfolio/6.png" alt="portfolio-image">
                        <div class="overlay work-info">
                            <div class="content d-flex flex-column justify-content-around">
                                <div class="info">
                                    <h2 class="title">WeChat App</h2>
                                    <span class="tag">Mobile app</span>
                                </div>
                                <div class="d-flex links justify-content-end">
                                    <a href="#"><i class="fas fa-eye"></i></a>
                                    <a href="#"><i class="fab fa-apple"></i></a>
                                    <a href="#"><i class="fab fa-google-play"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end portfolio item -->
        </div>

        <div class="text-center">
            <button class="btn main-btn white-btn px-5 mt-3">More</button>
        </div>
    </div>
</section>
<!-- end portfolio section -->

<!-- contact-us section -->
<section class="contact-us bg-cover" style="background-image: url('assets/images/contact-us-bg.png')">
<div class="container">
    @include('Front.partials.complaints')
</div>
</section>
<!-- end contact-us section -->

<!-- Modal -->
<div class="modal fade work-info-modal" id="work-info-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fas fa-times close-icon" data-dismiss="modal"></i>
            </div>
            <div class="modal-body">
                <!-- article section -->
                <section class="article">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-lg-6 m-b-30 article-cover">
                                <div class="article-image">
                                    <img src="assets/images/our-services/websites.png" alt="article-image">
                                </div>
                            </div>

                            <div class="col-lg-6 m-b-30 article-content">
                                <div>
                                    <h2 class="article-title">Mobile Apps</h2>
                                    <p class="article-text">Lorem Ipsum is simply dummy text of the printing and
                                        typesetting
                                        industry. Lorem Ipsum has been the industry's standard dummy text ever since
                                        the 1500s, when
                                        an unknown printer took a galley of type and scrambled it to make a type
                                        specimen book. It
                                        has survived not only five centuries, but also the leap into electronic
                                        typesetting,
                                        remaining essentially unchanged. It was popularised in the 1960s with the
                                        release of
                                        Letraset sheets containing Lorem</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- end article section -->

                <!-- article section -->
                <section class="article">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-lg-6 m-b-30 article-cover">
                                <div class="article-image">
                                    <img src="assets/images/our-services/websites.png" alt="article-image">
                                </div>
                            </div>

                            <div class="col-lg-6 m-b-30 article-content">
                                <div>
                                    <h2 class="article-title">Mobile Apps</h2>
                                    <p class="article-text">Lorem Ipsum is simply dummy text of the printing and
                                        typesetting
                                        industry. Lorem Ipsum has been the industry's standard dummy text ever since
                                        the 1500s, when
                                        an unknown printer took a galley of type and scrambled it to make a type
                                        specimen book. It
                                        has survived not only five centuries, but also the leap into electronic
                                        typesetting,
                                        remaining essentially unchanged. It was popularised in the 1960s with the
                                        release of
                                        Letraset sheets containing Lorem</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- end article section -->

                <!-- article section -->
                <section class="article">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-lg-6 m-b-30 article-cover">
                                <div class="article-image">
                                    <img src="assets/images/our-services/websites.png" alt="article-image">
                                </div>
                            </div>

                            <div class="col-lg-6 m-b-30 article-content">
                                <div>
                                    <h2 class="article-title">Mobile Apps</h2>
                                    <p class="article-text">Lorem Ipsum is simply dummy text of the printing and
                                        typesetting
                                        industry. Lorem Ipsum has been the industry's standard dummy text ever since
                                        the 1500s, when
                                        an unknown printer took a galley of type and scrambled it to make a type
                                        specimen book. It
                                        has survived not only five centuries, but also the leap into electronic
                                        typesetting,
                                        remaining essentially unchanged. It was popularised in the 1960s with the
                                        release of
                                        Letraset sheets containing Lorem</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- end article section -->
            </div>
        </div>
    </div>
</div>


<script src="assets/js/vendors/jquery-3.1.1.min.js"></script>
<script src="assets/js/vendors/slick.min.js"></script>
<script src="assets/js/vendors/bootstrap.min.js"></script>

<script src="assets/js/main.js"></script>
@endsection