@extends('Front.layouts.master')
@section('title', 'Our Team')
@section('content')
<!-- main section -->
<section class="main-section bg-cover d-flex align-items-center" style="background-image: url('assets/images/cover2.png')">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 mb-4 order-2 order-md-1">
                <div class="intro">
                    <h1 class="heading">TEAM</h1>
                    <p>Our works are designed by a team of experts whose goal is always to achieve the best results
                        ever.</p>
                </div>
            </div>
            <div class="col-md-6 mb-4 order-1 order-md-2">
                <div class="home-image">
                    <img src="assets/images/team/team.png" alt="team-image">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end main section -->

<!-- team section -->
<section class="team text-center">
    <div class="container">
        <div class="row">
            <!-- person info -->
            <div class="col-6 col-md-4 col-lg-3">
                <div class="person-info">
                    <div class="image-container">
                    <div class="person-image">
                        <img src="assets/images/team/1.png" alt="person-image">
                    </div>
                    </div>
                    <h3 class="person-name">Ahmed Ali</h3>
                    <p class="person-role">Graphic Designer</p>
                    <ul class="list-unstyled d-flex justify-content-center social-links">
                        <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- end person info -->

            <!-- person info -->
            <div class="col-6 col-md-4 col-lg-3">
                <div class="person-info">
                    <div class="image-container">
                    <div class="person-image">
                        <img src="assets/images/team/2.png" alt="person-image">
                    </div>
                    </div>
                    <h3 class="person-name">Ahmed Ali</h3>
                    <p class="person-role">Graphic Designer</p>
                    <ul class="list-unstyled d-flex justify-content-center social-links">
                        <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- end person info -->

            <!-- person info -->
            <div class="col-6 col-md-4 col-lg-3">
                <div class="person-info">
                    <div class="image-container">
                    <div class="person-image">
                        <img src="assets/images/team/3.png" alt="person-image">
                    </div>
                    </div>
                    <h3 class="person-name">Ahmed Ali</h3>
                    <p class="person-role">Graphic Designer</p>
                    <ul class="list-unstyled d-flex justify-content-center social-links">
                        <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- end person info -->

            <!-- person info -->
            <div class="col-6 col-md-4 col-lg-3">
                <div class="person-info">
                    <div class="image-container">
                    <div class="person-image">
                        <img src="assets/images/team/4.png" alt="person-image">
                    </div>
                    </div>
                    <h3 class="person-name">Ahmed Ali</h3>
                    <p class="person-role">Graphic Designer</p>
                    <ul class="list-unstyled d-flex justify-content-center social-links">
                        <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- end person info -->

            <!-- person info -->
            <div class="col-6 col-md-4 col-lg-3">
                <div class="person-info">
                    <div class="image-container">
                    <div class="person-image">
                        <img src="assets/images/team/1.png" alt="person-image">
                    </div>
                    </div>
                    <h3 class="person-name">Ahmed Ali</h3>
                    <p class="person-role">Graphic Designer</p>
                    <ul class="list-unstyled d-flex justify-content-center social-links">
                        <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- end person info -->

            <!-- person info -->
            <div class="col-6 col-md-4 col-lg-3">
                <div class="person-info">
                    <div class="image-container">
                    <div class="person-image">
                        <img src="assets/images/team/2.png" alt="person-image">
                    </div>
                    </div>
                    <h3 class="person-name">Ahmed Ali</h3>
                    <p class="person-role">Graphic Designer</p>
                    <ul class="list-unstyled d-flex justify-content-center social-links">
                        <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- end person info -->

            <!-- person info -->
            <div class="col-6 col-md-4 col-lg-3">
                <div class="person-info">
                    <div class="image-container">
                    <div class="person-image">
                        <img src="assets/images/team/3.png" alt="person-image">
                    </div>
                    </div>
                    <h3 class="person-name">Ahmed Ali</h3>
                    <p class="person-role">Graphic Designer</p>
                    <ul class="list-unstyled d-flex justify-content-center social-links">
                        <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- end person info -->

            <!-- person info -->
            <div class="col-6 col-md-4 col-lg-3">
                <div class="person-info">
                    <div class="image-container">
                    <div class="person-image">
                        <img src="assets/images/team/4.png" alt="person-image">
                    </div>
                    </div>
                    <h3 class="person-name">Ahmed Ali</h3>
                    <p class="person-role">Graphic Designer</p>
                    <ul class="list-unstyled d-flex justify-content-center social-links">
                        <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- end person info -->

            <!-- person info -->
            <div class="col-6 col-md-4 col-lg-3">
                <div class="person-info">
                    <div class="image-container">
                    <div class="person-image">
                        <img src="assets/images/team/1.png" alt="person-image">
                    </div>
                    </div>
                    <h3 class="person-name">Ahmed Ali</h3>
                    <p class="person-role">Graphic Designer</p>
                    <ul class="list-unstyled d-flex justify-content-center social-links">
                        <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- end person info -->

            <!-- person info -->
            <div class="col-6 col-md-4 col-lg-3">
                <div class="person-info">
                    <div class="image-container">
                    <div class="person-image">
                        <img src="assets/images/team/2.png" alt="person-image">
                    </div>
                    </div>
                    <h3 class="person-name">Ahmed Ali</h3>
                    <p class="person-role">Graphic Designer</p>
                    <ul class="list-unstyled d-flex justify-content-center social-links">
                        <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- end person info -->

            <!-- person info -->
            <div class="col-6 col-md-4 col-lg-3">
                <div class="person-info">
                    <div class="image-container">
                    <div class="person-image">
                        <img src="assets/images/team/3.png" alt="person-image">
                    </div>
                    </div>
                    <h3 class="person-name">Ahmed Ali</h3>
                    <p class="person-role">Graphic Designer</p>
                    <ul class="list-unstyled d-flex justify-content-center social-links">
                        <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- end person info -->

            <!-- person info -->
            <div class="col-6 col-md-4 col-lg-3">
                <div class="person-info">
                    <div class="image-container">
                    <div class="person-image">
                        <img src="assets/images/team/4.png" alt="person-image">
                    </div>
                    </div>
                    <h3 class="person-name">Ahmed Ali</h3>
                    <p class="person-role">Graphic Designer</p>
                    <ul class="list-unstyled d-flex justify-content-center social-links">
                        <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- end person info -->
        </div>
    </div>
</section>
<!-- end team section -->
<!-- contact-us section -->
<section class="contact-us bg-cover" style="background-image: url('assets/images/contact-us-bg.png')">
<div class="container">
    @include('Front.partials.complaints')
</div>
</section>
<!-- end contact-us section -->
@endsection