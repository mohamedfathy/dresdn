$(document).ready(function () {
    // ============================================  header ====================================
    function stickyHeader() {
        var header = $(".header");
        if ($(this).scrollTop() > 80) {
            header.addClass("sticky");
        } else {
            header.removeClass("sticky");
        }
    }
    stickyHeader();
    //sticky header
    $(window).on("scroll", function () {
        stickyHeader();
    });
    $(".header .navbar-toggler").addClass("collapsed");

    // scroll down btn
    $(".scroll-btn").on("click", function () {
        $("html").animate({
            scrollTop: $(".main-section").outerHeight()
        })
    });

    // ============================================  testimonials slider ====================================
    $('.testimonials-slider').slick({
        autoplay: true,
        speed: 800,
        autoplaySpeed: 5000,
        nextArrow: "<div class='next-arrow d-none d-md-block'><i class='fa fa-chevron-right'></i></div>",
        prevArrow: "<div class='pev-arrow d-none d-md-block'><i class='fa fa-chevron-left'></i></div>",
        dots: true,
    });


    // ==================================== portfolio  filter ====================================
    $(".filter-buttons button").on("click", function () {
        var filterVal = $(this).data("filter");

        if (filterVal == "*") {
            $('.portfolio-items').find(".item").addClass("scale-in-center").show();
            setTimeout(function () {
                $('.portfolio-items').find(".item").removeClass("scale-in-center");
            }, 500);
        } else {
            $('.portfolio-items').find(filterVal).addClass("scale-in-center").show().siblings().not(filterVal).hide().removeClass("scale-in-center");
            setTimeout(function () {
                $('.portfolio-items').find(".item").removeClass("scale-in-center");
            }, 500);
        }
    });
    //add class active to filter btn
    $(".filter-buttons button").click(function () {
        $(this).addClass("active").siblings().removeClass("active");
    });
});